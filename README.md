# greentext neural network

a simple neural network that generates greentexts, developed with char-rnn: https://github.com/karpathy/char-rnn

to run:
1) first follow all instructions to install char-rnn (you will also need to install torch and luarocks), save 'greentext-nn.t7' 
to the 'cv' folder in your 'char-rnn' directory

2) use command "cd char-rnn"

3) use command "th sample.lua cv/greentext-nn.t7 -primetext <text here> -gpuid -1"